console.log('hi')

let getCube = 2 ** 3;
console.log(`The cube of 2 is: ${getCube}`);

let address = [12, "Sires Parkway", "Auckland","NZ"];
const [streetNumber, streetName, townName, countryName] = address;

console.log(`I live at number ${streetNumber} ${streetName}, ${townName}, ${countryName}`)

const animal = {
	nickname: "Lolong",
	weight: "1075 kgs",
	length: "20 ft 3 inches"
};

const {nickname, weight, length} = animal
console.log(`${nickname} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${length}`);

let numbers = [1, 2, 3, 4, 5, 15];

numbers.forEach((number) => {
	console.log(number)
});

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age; 
		this.breed = breed;
	};
};

const myDog = new Dog;
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Daschund"
console.log(myDog);